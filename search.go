package command

import (
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/search"
)

// Search returns a cli sub-command that implements project search.
func Search(cfg Config) *cli.Command {
	return &cli.Command{
		Name:      "search",
		Aliases:   []string{"s"},
		Usage:     "Search for compatible projects and return project directory",
		ArgsUsage: "<path>",
		Flags:     append(cacert.NewFlags(), search.NewFlags()...),
		Action: func(c *cli.Context) error {
			// check args
			if c.Args().Len() != 1 {
				if err := cli.ShowSubcommandHelp(c); err != nil {
					return err
				}
				return errInvalidArgs
			}
			rootPath := c.Args().First()

			// import CA bundle
			if err := cacert.Import(c, cfg.CACertImportOptions); err != nil {
				return err
			}

			// search
			log.Info("Detecting project")
			opts := search.NewOptions(c)
			matchPath, err := search.New(cfg.Match, opts).Run(rootPath)
			if err != nil {
				return err
			}
			log.Info(matchPath)
			return nil
		},
	}
}
